﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BabyStore.WebUI.Startup))]
namespace BabyStore.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
