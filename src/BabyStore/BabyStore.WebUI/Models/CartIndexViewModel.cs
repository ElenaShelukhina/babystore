﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BabyStore.Domain.Entities;

namespace BabyStore.WebUI.Models
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}